package demos;

import org.junit.Assert;
import org.junit.Test;

public class TestClass01 extends DemoMath{

    DemoMath demoMath=new DemoMath();

    @Test
    public void verifyAdd(){
        int result=demoMath.sum(15,20);
        Assert.assertEquals(35,result);

    }
}
